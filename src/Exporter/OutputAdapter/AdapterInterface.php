<?php

namespace Sparkson\DataExporterBundle\Exporter\OutputAdapter;

use Sparkson\DataExporterBundle\Exporter\Column\Column;
use Sparkson\DataExporterBundle\Exporter\Column\ColumnInterface;

/**
 * Interface for exporter output adapter.
 *
 * @author Tamcy <tamcyhk@outlook.com>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
interface AdapterInterface
{
    /**
     * Called when the exporter begins exporting data.
     * 
     * @return void
     */
    public function begin();

    /**
     * Called when the exporter exports a record.
     *
     * @param ColumnInterface[] $columns The column set
     * @param array<string,mixed> $record The record
     * 
     * @return void
     */
    public function writeRecord(array $columns, array $record);

    /**
     * Called when the exporter finishes exporting data.
     * 
     * @return void
     */
    public function end();

    /**
     * Returns the buffered result produced by this output adapter.
     *
     * @return string|false|null
     */
    public function getResult();
}