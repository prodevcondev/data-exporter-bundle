<?php

namespace Sparkson\DataExporterBundle\Exporter\OutputAdapter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Sparkson\DataExporterBundle\Exporter\Column\Column;

/**
 * CSV output adapter.
 *
 * @author Tamcy <tamcyhk@outlook.com>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class CSVAdapter extends BaseFlattenAdapter
{
    /**
     * Handle
     *
     * @var resource|false
     */
    protected $handle;

    /**
     * Data
     *
     * @var string|false|null
     */
    protected $data;

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'filename' => 'php://temp/maxmemory:5242880',
            'keep_result' => true,
            'delimiter' => ',',
            'enclosure' => '"',
            'escape_char' => '\\',
        ));

        $resolver->setAllowedTypes('keep_result', 'bool');
    }

    /**
     * {@inheritdoc}
     */
    public function begin()
    {
        parent::begin();
        $this->handle = fopen($this->options['filename'], 'w+');
        $this->data = null;
    }

    /**
     * {@inheritdoc}
     */
    protected function writeHeaderRow(array $columns)
    {
        $labels = array();
        /**
         * @var string $key
         * @var Column $column
         */
        foreach ($columns as $key => $column) {
            $labels[$key] = $column->getLabel();
        }

        if (! $this->handle) {
            return;
        }

        fputcsv($this->handle, $labels, $this->options['delimiter'], $this->options['enclosure'], $this->options['escape_char']);
    }

    /**
     * {@inheritdoc}
     */
    protected function writeRecordRow(array $columns, array $record)
    {
        $fields = array();

        foreach ($columns as $key => $columnLabel) {
            $fields[] = $record[$key];
        }

        if (! $this->handle) {
            return;
        }

        fputcsv($this->handle, $fields, $this->options['delimiter'], $this->options['enclosure'], $this->options['escape_char']);
    }

    /**
     * {@inheritdoc}
     */
    public function end()
    {
        if (! $this->handle) {
            return;
        }

        if ($this->options['keep_result']) {
            rewind($this->handle);
            $this->data = stream_get_contents($this->handle);
        }

        fclose($this->handle);
    }

    /**
     * {@inheritdoc}
     */
    public function getResult()
    {
        return $this->data;
    }
}