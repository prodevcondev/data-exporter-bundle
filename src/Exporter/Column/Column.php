<?php

namespace Sparkson\DataExporterBundle\Exporter\Column;

use Sparkson\DataExporterBundle\Exporter\Type\ExporterTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A column.
 *
 * @author Tamcy <tamcyhk@outlook.com>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
class Column extends AbstractColumnContainer implements ColumnInterface
{
    /**
     * The column name, which is unique in the column set.
     *
     * @var string
     */
    private $name;

    /**
     * The value type of this column.
     *
     * @var ExporterTypeInterface
     */
    private $type;

    /**
     * Column and type options
     *
     * @var array<string,mixed>
     */
    private $options;

    /**
     * Whether this column is enabled.
     *
     * @var boolean
     */
    private $enabled = true;

    /**
     * The position of this column in the column set.
     *
     * @var int
     */
    private $position;

    /**
     * Consctructor
     *
     * @param string $name
     * @param ExporterTypeInterface $type
     * @param array<string,mixed> $options
     */
    public function __construct($name, ExporterTypeInterface $type, array $options = array())
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'label' => null,
            'output_options' => array(), // column options specified for the output adapter
        ]);

        $resolver->setAllowedTypes('output_options', 'array');

        $type->setDefaultOptions($resolver);

        $this->options = $resolver->resolve($options);

        $this->name = $name;
        $this->type = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel(): string
    {
        if ($this->options['label']) {
            return $this->options['label'];
        }

        return ucwords(ltrim(str_replace("_", " ", preg_replace('/([^A-Z])([A-Z])/', "\\1 \\2", $this->getName()))));
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): ExporterTypeInterface
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * {@inheritdoc}
     */
    public function setOptions($options): void
    {
        $this->assertNotLocked();
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($enabled): void
    {
        $this->assertNotLocked();
        $this->enabled = $enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function setPosition($position): void
    {
        $this->assertNotLocked();
        $this->position = $position;
    }
}