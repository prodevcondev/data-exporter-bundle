<?php

namespace Sparkson\DataExporterBundle\Exporter\Column;


use Sparkson\DataExporterBundle\Exporter\Exception\ColumnNotFoundException;
use Sparkson\DataExporterBundle\Exporter\Exception\InvalidOperationException;

/**
 * The base column set class.
 *
 * @author Tamcy <tamcyhk@outlook.com>
 */
abstract class AbstractColumnContainer implements ColumnCollectionInterface
{
    /**
     * @var Column[]|null Array of columns, with column name as key
     */
    protected $children;

    /**
     * @var ColumnInterface[] Array of built columns
     */
    protected $sortedColumns;

    /**
     * @var bool True if the column set is built for export and locked
     */
    protected $locked = false;

    /**
     * {@inheritdoc}
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    protected function assertNotLocked(): void
    {
        if ($this->locked) {
            throw new InvalidOperationException("Cannot modify a locked column set");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren(): bool
    {
        return !!$this->children;
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(ColumnInterface $column): void
    {
        $this->assertNotLocked();

        if (null === $this->children) {
            $this->children = [];
        }

        if (!isset($this->children[$column->getName()])) {
            $position = count($this->children);
        } else {
            $position = $column->getPosition();
        }
        $column->setPosition($position);
        $this->children[$column->getName()] = $column;
    }

    /**
     * {@inheritdoc}
     */
    public function getChild($columnName): ColumnInterface
    {
        if (isset($this->children[$columnName])) {
            return $this->children[$columnName];
        };

        throw new ColumnNotFoundException($columnName);
    }

    /**
     * {@inheritdoc}
     */
    public function hasChild($columnName): bool
    {
        return isset($this->children[$columnName]);
    }

    /**
     * {@inheritdoc}
     */
    public function removeChild($columnName): void
    {
        if ($this->hasChild($columnName)) {
            unset($this->children[$columnName]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setColumnOrders(array $columnNames, $disableOtherColumns = false): void
    {
        $allColumnNames = array_flip(array_keys($this->children));

        foreach ($columnNames as $position => $columnName) {
            $this->getChild($columnName)->setPosition($position + 1);
            unset($allColumnNames[$columnName]);
        }

        if ($disableOtherColumns) {
            foreach ($allColumnNames as $columnName => $dummy) {
                $this->getChild($columnName)->setEnabled(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function build(): void
    {
        if ($this->hasChildren()) {

            /** @var Column[] $columns */
            $columns = array_values(array_filter($this->children, function (ColumnInterface $col) {
                return $col->isEnabled();
            }));

            usort($columns, function (ColumnInterface $colA, ColumnInterface $colB) {
                return $colA->getPosition() - $colB->getPosition();
            });

            foreach ($columns as $column) {
                $column->build();
            }
            $this->sortedColumns = $columns;
        }
        $this->locked = true;
    }

    /**
     * {@inheritdoc}
     */
    public function getBuiltColumns(): array
    {
        return $this->sortedColumns;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset): bool
    {
        return $this->hasChild((string) $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->getChild((string) $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        throw new InvalidOperationException('Calling method offsetSet() of ColumnCollectionInterface is not allowed.');
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->removeChild((string) $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->children);
    }

}