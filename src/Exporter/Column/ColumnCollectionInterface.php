<?php

namespace Sparkson\DataExporterBundle\Exporter\Column;

/**
 * Interface for a column set.
 *
 * @extends \ArrayAccess<int,ColumnInterface>
 * 
 * @author Tamcy <tamcyhk@outlook.com>
 * @author Vladimir Simic <vladimir.simic@prodevcon.ch>
 */
interface ColumnCollectionInterface extends \ArrayAccess, \Countable
{
    /**
     * Replaces the containing columns with the supplied one.
     *
     * @param Column[] $children The array of columns to replace
     * 
     * @return void
     */
    public function setChildren(array $children): void;

    /**
     * Returns whether this column set has at least one child.
     *
     * @return bool
     */
    public function hasChildren();

    /**
     * Returns the children of this column set.
     *
     * @return ColumnInterface[]
     */
    public function getChildren(): array;

    /**
     * Adds a column to the end of the column set.
     *
     * Column name must be unique in the column set, otherwise existing column
     * with the same column name will be overwritten.
     *
     * @param ColumnInterface $column The column to add
     * 
     * @return void
     */
    public function addChild(ColumnInterface $column): void;

    /**
     * Retrieves the child column by its name.
     *
     * @param string $columnName Name of the column
     * 
     * @return ColumnInterface
     */
    public function getChild($columnName): ColumnInterface;

    /**
     * Returns whether a column with the supplied column name exists.
     *
     * @param string $columnName Name of the column
     * 
     * @return bool True if the column is found, other false
     */
    public function hasChild($columnName): bool;

    /**
     * Removes a column from the column set.
     *
     * @param string $columnName Name of the column
     * 
     * @return void
     */
    public function removeChild($columnName): void;

    /**
     * Builds the column set.
     *
     * This method will build the column set array ready for export use. The resulting column set array will contain
     * only enabled columns, sorted by their assigned positions. The column set will be read-only after this method is
     * run.
     * 
     * @return void
     */
    public function build(): void;

    /**
     * Returns the column set built by the build() method.
     *
     * @see build
     * @return ColumnInterface[] An array of columns ready for export use
     */
    public function getBuiltColumns(): array;

    /**
     * Assigns the position of each column according to the order of the supplied array.
     *
     * This is a helper method so that you don't need to write a bunch of
     * $columnSet->getChild('columnName')->setPosition(...).
     * When $disableOtherColumns is true, columns not specified in $columnNames will be disabled, so that they won't
     * show up in the exported document.
     * Note: columns specified in $columnNames will NOT be enabled explicitly, even when $disableOtherColumns is true.
     *
     * @param string[] $columnNames
     * @param bool  $disableOtherColumns
     * 
     * @return void
     */
    public function setColumnOrders(array $columnNames, $disableOtherColumns = false): void;
}